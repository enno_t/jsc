# jsc - A Johnny Simulator Compiler

## What this does

This 'compiler' transforms the pseudo-assembly of Johnny Simulator into the .ram
files it expects, allowing users to write programs in a more comfortable form.

Thus a JS-Program like

``` 
INC 20
TAKE 20
ADD 40
SAVE 50
HLT
```

becomes the .ram file

```
7020
1020
2040
4050
10000
```

## How big is it?

Language|files|blank|comment|code
:-------|-------:|-------:|-------:|-------:
.c|8|243|571|744
.h|6|41|96|123
--------|--------|--------|--------|--------
SUM:|14|284|667|867

stats by [cloc](https://github.com/AlDanial/cloc)
