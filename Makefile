CC ?= gcc
CFLAGS ?= -Wall -Wextra -std=c99 \
       -pipe -march=native -O2 \
       -D_FORTIFY_SOURCE=2 \
       -fstack-protector-strong \
       -fcf-protection -pedantic \
       -pedantic-errors

LDFLAGS ?= -Wl,-z,defs -Wl,-z,now -Wl,-z,relro

OBJQ = main.o input.o asm.o ast.o helpers.o errorlist.o check.o jalloc.o

VALGRIND_FLAGS ?= --leak-check=full --show-leak-kinds=all \
		  -s --track-origins=yes --time-stamp=yes

%.o: src/%.c
	@echo CC $^
	@$(CC) -c -o $@ $< $(CFLAGS)

jsc: $(OBJQ)
	@echo CC $^
	@$(CC) -o $@ $^ $(CLFAGS) $(LDFLAGS)

test: jsc
	valgrind $(VALGRIND_FLAGS) ./jsc test.johnny -o /tmp/out
	valgrind $(VALGRIND_FLAGS) ./jsc test.johnny -p -o /tmp/out
	valgrind $(VALGRIND_FLAGS) ./jsc test.johnny
	valgrind $(VALGRIND_FLAGS) ./jsc test.johnny -p

.PHONY: clean
clean:
	@rm -f jsc *.o

