// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef INPUT_H
#define INPUT_H

#include <sys/types.h>

#define JSC_TOK_BUF 64
#define JSC_TOK_DELIM " \t\r\n\a"

char *jsc_read_file(const char *);
char **jsc_tokenize_input(char *);
int jsc_write_file(const char *, char *);
int jsc_append_line(const char *, char *);
int jsc_write_lines(const char *, char **);

// Helper functions
off_t jsc_file_size(const char *);
size_t jsc_tokens_size(char **);

#endif
