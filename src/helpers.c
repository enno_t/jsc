// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "helpers.h"
#include "jalloc.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * is_digit() - Check if sth is a digit
 *
 * @s: the cstring to check
 *
 * Return:
 * * 0 if it is not a digit
 * * 1 if it is a digit
 *
 * As long as the cstring contains any number it is, for our purposes, a digit
 */
int is_digit(char *s)
{
	int ret = 0;
	for (int i = 0; s[i] != '\0'; ++i) {
		ret += IS_NUMBER(s[i]);
	}

	return ret != 0;
}

/**
 * create_padding() - Create lots of rows containing 0000
 *
 * @num_lines: Number of lines to create
 *
 * Returns: 
 * * "0000" * num_lines
 * * NULL on error
 */
char *create_padding(int num_lines)
{
	char *padding = "0000\n";
	char *buf = j_alloc(strlen(padding) * num_lines + 1, sizeof(char));

	if (!buf) {
		perror("jsc - create_pading");

		return NULL;
	}

	for (int i = 0; i < num_lines; ++i)
		strcat(buf, padding);

	return buf;
}

/**
 * fnv1a_hash() - Calculate a simple FNV1A-hash
 *
 * @s: The cstring to hash
 *
 * The FNV1A-Hash is a simple, easy-to-implement hashing function that does
 * all we need for internal hashing, as that is what is designed to be used
 * for. See Fowler-Noll-Vo hash function on the 'pedia
 *
 * Returns: the hash as a long int
 */
long fnv1a_hash(const char *s)
{
	long hash = FNV1A_OFFSET;

	for (size_t i = 0; s[i] != '\0'; ++i) {
		hash ^= s[i];
		hash *= FNV1A_PRIME;
	}

	return hash;
}

/**
 * divide() - Divide two integers
 *
 * @dividend: The dividend
 * @divisor: The divisor
 *
 * Return: CEIL(dividend / divisor)
 */
int divide(int dividend, int divisor)
{
	double tmp = dividend / (double)divisor;
	int ret = (int)tmp;
	if ((double)ret < tmp)
		return ++ret;
	else
		return ret;
}
