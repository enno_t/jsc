// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "asm.h"
#include "helpers.h"
#include "jalloc.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

static const char *OPCODE_STRS[] = { "",     "TAKE", "ADD", "SUB",
				     "SAVE", "JMP",  "TST", "INC",
				     "DEC",  "NULL", "HLT" };

/**
 * instruction_create() - Create a instruction struct
 *
 * @ins: The given instruction
 * @target: The argument to that instruction, if any
 *
 * This uses other functions, like str_to_opcode(), to create a structure
 * representing the given instruction-target pair. There is the edge case
 * that when HLT does not have a defined target, as it does not technically
 * need one, @target can be NULL.
 *
 * Return:
 * * the created structure
 * * NULL on failure
 */
struct instruction *instruction_create(char *ins, char *target)
{
	struct instruction *new = j_alloc(1, sizeof(struct instruction));

	if (!new) {
		perror("jsc - instruction_create");

		return NULL;
	}

	new->ins = str_to_opcode(ins);
	if (new->ins == INS_SIZE)
		new->ins = INS_DATA;

	char *tmp;
	if (target) {
		new->target = strtol(target, &tmp, 0);
		if (target == tmp || errno != 0)
			new->target = INVALID_TARGET;
	} else {
		new->target = NO_TARGET;
	}

	return new;
}

/**
 * opcode_to_str() - Return the string of any given opcode
 *
 * @ins: the given opcode
 *
 * This is just for readabilitie, there are no cases to consider. It is a direct
 * call to OPCODE_STRS[ins], since we can do that.
 *
 * Return: a cstring
 */
const char *opcode_to_str(enum opcode ins)
{
	return OPCODE_STRS[ins];
}

/**
 * instruction_to_str() - Turn an instruction struct into a cstring
 *
 * @i: The given instruction
 *
 * This basically reverts instruction_create()
 *
 * Return:
 * * the string representation of the given instruction
 * * NULL on any error
 */
char *instruction_to_str(struct instruction *i)
{
	char *buf = j_alloc(65, sizeof(char));

	if (!buf) {
		perror("jsc - instruction_to_str");

		return NULL;
	}

	const char *op = OPCODE_STRS[i->ins];

	char *target = int_to_str(i->target, 3);

	if (!target)
		return NULL;

	strcat(buf, op);
	strcat(buf, " ");
	strcat(buf, target);

	return buf;
}

/**
 * str_to_opcode() - Return the enum member corresponding to any given cstring
 *
 * @s: the given cstring
 *
 * Using fnv1a_hash() we see which opcode matches the given cstring.
 *
 * Return:
 * * the matching opcode
 * * INS_SIZE, if no match was found
 */
enum opcode str_to_opcode(char *s)
{
	long hash = fnv1a_hash(s);

	for (int i = 0; i < INS_SIZE; ++i) {
		if (hash == fnv1a_hash(OPCODE_STRS[i])) {
			return (enum opcode)i;
		}
	}

	return INS_SIZE;
}

/**
 * asm_compile() - Turn an instruction into the johhny-bytecode
 *
 * @i: the given instruction
 *
 * We allocate 16 bytes for the new cstring, turn the parts of the instruction
 * into their num-strings using int_to_str() and then concatenate them.
 *
 * Return:
 * * the created bytecode-string
 * * NULL on any error
 */
char *asm_compile(struct instruction *i)
{
	char *buf = j_alloc(LINE_SIZE, sizeof(char));

	if (!buf) {
		perror("jsc - asm_compile");

		return NULL;
	}

	char *op = int_to_str(i->ins, 0);
	int tmp = (IS_VALID(i->target)) ? i->target : 0;

	char *target = int_to_str(tmp, 3);

	if (!op || !target)
		return NULL;

	strcat(buf, op);
	strcat(buf, target);

	return buf;
}

/**
 * int_to_str() - Turn an integer into its cstring representation
 *
 * @i: the given integer
 * @padding: Should the number be 0 padded?
 *
 * Using snprintf, we calculate the needed size for the num-string and then
 * allocate that memory and format the number (optionally 0-padded, if padding
 * is greater than 0).
 *
 * Return:
 * * @i as a cstring
 * * NULL on any error
 */
char *int_to_str(int i, int padding)
{
	int len;
	if (!padding)
		len = snprintf(NULL, 0, "%d", i) + 1;
	else
		len = snprintf(NULL, 0, "%0*d", padding, i) + 1;

	char *buf = j_alloc(len + 1, sizeof(char));

	if (!buf) {
		perror("jsc - int_to_str");

		return NULL;
	}

	if (!padding)
		snprintf(buf, len, "%d", i);
	else
		snprintf(buf, len, "%0*d", padding, i);

	return buf;
}
