// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef ASM_H
#define ASM_H

/**
 * enum opcode - All possible opcodes, ordered with their values
 *
 * @INS_DATA: 0 or no opcode
 * @INS_TAKE: 1 or TAKE
 * @INS_ADD: 2 or ADD
 * @INS_SUB: 3 or SUB
 * @INS_SAVE: 4 or SAVE
 * @INS_JMP: 5 or JMP
 * @INS_TST: 6 or TST
 * @INS_INC: 7 or INC
 * @INS_DEC: 8 or DEC
 * @INS_NULL: 9 or NULL
 * @INS_HLT: 10 or HLT
 * @INS_SIZE: 11 or how many members this enum has
 */
enum opcode {
	INS_DATA,
	INS_TAKE,
	INS_ADD,
	INS_SUB,
	INS_SAVE,
	INS_JMP,
	INS_TST,
	INS_INC,
	INS_DEC,
	INS_NULL,
	INS_HLT,
	INS_SIZE
};

#define IS_VALID(x) (x >= 0)
#define INVALID_TARGET -1
#define NO_TARGET -2

/**
 * struct instruction - an instruction broken down to its essentials
 * 
 * @ins: Which instruction is this
 * @target: What address does it target
 *
 * Any instruction will have an opcode (@ins) and a target (@target), even HLT
 * (which does ignore the target, so it's usually 0)
 */
struct instruction {
	enum opcode ins;
	int target;
};

struct instruction *instruction_create(char *, char *);
const char *opcode_to_str(enum opcode);
enum opcode str_to_opcode(char *);
char *int_to_str(int, int);
char *instruction_to_str(struct instruction *);
char *asm_compile(struct instruction *);

#endif
