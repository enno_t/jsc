// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "ast.h"
#include "helpers.h"
#include "jalloc.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * ast_create() - Create an empty ast structure
 *
 * @num_lines: Linecount of the parsed source file
 *
 * We need a way to store all instructions so that we can later access them to
 * turn them into the bytecode. For this we use an array of the size we need.
 *
 * @num_lines is not actually the number of lines, but the number of tokens, so
 * either 2 or _juuust_ under two times the actual line count, so we have to
 * account for that.
 *
 * __len is 0 at the start, since that counts how many elemtents are currently
 * in the array.
 *
 * Return:
 * * an ast_struct
 * * NULL on any error
 */
struct ast *ast_create(int num_lines)
{
	struct ast *a = j_alloc(1, sizeof(struct ast));

	if (!a) {
		perror("jsc - ast_create");

		return NULL;
	}

	a->num_lines = divide(num_lines, 2);

	a->__len = 0;

	a->lines = j_alloc(num_lines, sizeof(struct instruction *));

	if (!a->lines) {
		perror("jsc - ast_create");
		return NULL;
	}

	return a;
}

/**
 * ast_add_instruction() - Add an instruction to the lines array
 *
 * @a: the ast struct
 * @i: the instruction that we want to add
 *
 * We both know how many elemtents we can and do hold, so this is a very simple
 * function. We check if we have reached the limit and if not we add the
 * instruction to the array.
 *
 * This fails silently, if we exceed the allocated array, but that should not
 * really happen, so that is fine.
 */
void ast_add_instruction(struct ast *a, struct instruction *i)
{
	if (a->__len < a->num_lines) {
		a->lines[a->__len] = i;

		a->__len += 1;
	}
}

/**
 * ast_compile() - Compile all instructions to bytecode
 *
 * @a: the ast struct holding all instructions
 *
 * We allocate 16 bytes times however many instructions we have and call asm_compile()
 * for each of them. We concatenate all of the bytecode-strings with newlines.
 *
 * Return:
 * * the bytecode for all instructions
 * * NULL on any error
 *
 * See asm_compile()
 */
char *ast_compile(struct ast *a)
{
	if (ast_find_ins(a, INS_HLT) < 0) {
		fprintf(stderr,
			"jsc - ast_compile: Source contained no HLT instruction.");
		return NULL;
	}

	char *buf = j_alloc(LINE_SIZE * a->num_lines + 1, sizeof(char));

	if (!buf) {
		perror("jsc - ast_compile");

		return NULL;
	}

	char *s;

	int i;
	ast_foreach_instruction(i, a) {
		s = asm_compile(a->lines[i]);

		if (!s)
			return NULL;

		strcat(buf, s);
		strcat(buf, "\n");
	}

	return buf;
}

/**
 * int ast_find_ins() - Search for the first instance of an instruction
 *
 * @a: all current entries
 * @ins: the instruction to look for
 *
 * Loop through all entries of a and check if ins is a member of it.
 *
 * Return:
 * * the index of the first instance of the instruction
 * * -1 if none are found
 */
int ast_find_ins(struct ast *a, enum opcode ins)
{
	int i = 0;

	ast_foreach_instruction(i, a) {
		if (a->lines[i]->ins == ins) {
			return i;
		}
	}

	return -1;
}

/**
 * ast_check() - Check each instruction for correctness
 *
 * @a: The ast struct
 * @l: The list of errors
 *
 * Iterate of all instructions and call all check_* functions with
 * them.
 */
void ast_check(struct ast *a, struct error_list *l)
{
	int line = 0;
	struct instruction *tmp;
	ast_foreach_instruction(line, a) {
		tmp = a->lines[line];
		check_address(l, line + 1, (int)tmp->ins, tmp->target);
		check_opcode(l, line + 1, (int)tmp->ins, tmp->target);
		check_target(l, line + 1, (int)tmp->ins, tmp->target);
	}
}
