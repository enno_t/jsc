// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef AST_H
#define AST_H
#include "asm.h"
#include "check.h"

#define ast_check_bounds(i, x) \
	(i < x->num_lines && i < x->__len)

#define ast_foreach_instruction(i, x) \
	for (i = 0; ast_check_bounds(i, x); ++i)

/**
 * struct ast - wrapper around an struct instruction array
 * 
 * @__len: internal element count
 * @num_lines: maximum possible elements
 * @lines: array of instructions
 */
struct ast {
	int __len;
	int num_lines;
	struct instruction **lines;
};

struct ast *ast_create(int);
void ast_add_instruction(struct ast *, struct instruction *);
int ast_find_ins(struct ast *, enum opcode);
char *ast_compile(struct ast *);
void ast_check(struct ast *, struct error_list *);


#endif
