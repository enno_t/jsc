// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

#include "input.h"
#include "jalloc.h"

/**
 * jsc_file_size() - Uses sys/stat.h to get the size of the file at path
 *
 * @path: Path of the file
 *
 * We use sys/stat.h to get the size of the file. It's easier than the normal
 * fseek() stuffs.
 *
 * Return:
 * * The size of the file
 * * -1 on error
 */
off_t jsc_file_size(const char *path)
{
	struct stat st;

	if (stat(path, &st) == 0)
		return st.st_size;

	return -1;
}

/**
 * jsc_read_file() - Read the contents of a file
 *
 * @path: Path of the file we want to read
 *
 * We get the filesize to know how much space to allocate and then read the
 * entire file.
 *
 * Return:
 * * The conents of the file
 * * NULL on error
 *
 * See jsc_file_size()
 */
char *jsc_read_file(const char *path)
{
	off_t file_size = jsc_file_size(path);

	if (file_size == -1) {
		perror("jsc_read_file");

		return NULL;
	}

	FILE *fp = fopen(path, "r");

	if (!fp) {
		perror("jsc_read_file");

		return NULL;
	}

	char *input = j_alloc(file_size + 1, sizeof(char));

	if (!input)
		goto error;

	int num_read = fread(input, sizeof(char), file_size, fp);

	if (num_read < file_size)
		goto error;

	if (ferror(fp))
		goto error;

	fclose(fp);

	return input;

error:
	perror("jsc_read_file");

	fclose(fp);

	return NULL;
}

/**
 * jsc_tokenize_input - Turn input from string to array of tokens
 *
 * @input: Our stirng of tokens
 *
 * Using strtok() we split at all whitespace and save the tokens in an array.
 *
 * Return:
 * * array of tokens
 * * NULL on error
 */
char **jsc_tokenize_input(char *input)
{
	off_t size = JSC_TOK_BUF;

	char **tokens = j_alloc(size, sizeof(char *));

	if (!tokens)
		goto error;

	off_t pos = 0;

	char *token = strtok(input, JSC_TOK_DELIM);

	while (token != NULL) {
		tokens[pos] = token;
		++pos;

		if (pos >= size) {
			size += JSC_TOK_BUF;
			tokens = j_realloc(tokens, size, sizeof(char *));

			if (!tokens)
				goto error;
		}

		token = strtok(NULL, JSC_TOK_DELIM);
	}

	tokens[pos] = NULL;

	return tokens;

error:
	perror("jsc_tokenize_input");
	return NULL;
}

/**
 * jsc_write_file() - Write contents to a file
 *
 * @path: Destination for the conents
 * @data: The conents to write
 *
 * We write all bytes in @data to @path and return.
 *
 * Return:
 * * 0 - Success
 * * -1 - Could not open @path
 * * -2 - Could not write all of @data
 */
int jsc_write_file(const char *path, char *data)
{
	size_t data_size = strlen(data);

	FILE *fp = fopen(path, "w+");

	if (!fp) {
		perror("jsc_write_file");
		return -1;
	}

	size_t written = fwrite(data, sizeof(char), data_size, fp);

	if (written < data_size) {
		perror("jsc_write_file");
		fclose(fp);
		return -2;
	}

	fclose(fp);
	return 0;
}

/**
 * jsc_write_lines() - jsc_write_file for cstring-arrays
 *
 * @path: Destination for the content
 * @lines: Array of cstrings (Last entry must be NULL)
 *
 * For every member of @lines we call jsc_append_line() and append it to the file
 * at @path.
 *
 * Return:
 * * 0 - Success
 * * -1 - Could not open @path
 * * -2 - Could not write all of some member of @lines
 *
 * See jsc_append_line()
 */
int jsc_write_lines(const char *path, char **lines)
{
	int ret = 0;
	size_t num_lines = jsc_tokens_size(lines);

	for (size_t i = 0; i < num_lines; ++i) {
		ret = jsc_append_line(path, lines[i]);

		if (ret < 0)
			return ret;
	}

	return 0;
}

/**
 * jsc_append_line() - Append a line to a file
 *
 * @path: The file to append to
 * @line: The data to append
 *
 * We open the file with mode a+ and then write @line
 *
 * Return:
 * * 0 - Success
 * * -1 - Could not open @path
 * * -2 - Could not write all of @line
 */
int jsc_append_line(const char *path, char *line)
{
	size_t data_size = strlen(line);

	FILE *fp = fopen(path, "a+");

	if (!fp) {
		perror("jsc_append_line");
		return -1;
	}

	size_t written = fwrite(line, sizeof(char), data_size, fp);

	if (written < data_size) {
		perror("jsc_append_line");
		fclose(fp);

		return -2;
	}

	fclose(fp);

	return 0;
}

/**
 * jsc_token_size() - Count the members of a cstring-array
 *
 * @tokens: the cstring array
 *
 * We assume @tokens to be NULL-terminanted, i.e. the last member of @tokens is
 * NULL. If the last member of @tokens is not NULL ub happens.
 *
 * Return: Number of members @tokens has
 */
size_t jsc_tokens_size(char **tokens)
{
	size_t i = 0;
	for (; tokens[i] != NULL; ++i)
		;

	return i;
}
