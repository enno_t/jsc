// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "input.h"
#include "asm.h"
#include "ast.h"
#include "check.h"
#include "helpers.h"
#include "jalloc.h"

/**
 * struct options - structure holding all args to the program
 *
 * @in: The sourcefile
 * @out: The destination file
 * @pad: should we add padding?
 * @just_check: Should we exit after checking for errors?
 * @dont_check: Should we check for errors?
 * @ignore: Shoule we ignore errors?
 *
 * Since padding is optional, we can't just do a simple argv[1] is in and argv[2]
 * is out. Or at least, not without having an awful usage. So create this and a
 * option parsing function instead :)
 */
struct options {
	char *in;
	char *out;
	int pad;
	int just_check;
	int dont_check;
	int ignore;
};

int compile(struct options *);
char *compile_source(char *, int, int, int);
char *create_ram_string(char *);
struct options *parse(int, char **);
char *last_standalone(int, char **);
void usage(void);

static int TOKEN_SIZE = 0;

/**
 * compile() - Wrapper for bytecode generation
 *
 * @opts: All given options
 *
 * We turn the contents of @opts->in into bytecode, we optionally pad it with all
 * zero cstrings until its length is 1000 lines (if @opts->pad is non-zero) and write
 * the result to @opts->out.
 *
 * Return:
 * * 0 - Success
 * * 1 - Failure
 */
int compile(struct options *opts)
{
	char *infile = opts->in;
	char *outfile = opts->out;
	char *compiled_file =
		compile_source(infile, opts->just_check,
				opts->dont_check, opts->ignore);
	char *ram_file = NULL;

	if (!compiled_file)
		return 1;

	if (opts->pad) {
		ram_file = create_ram_string(compiled_file);
		jsc_write_file(outfile, ram_file);

		if (ram_file == compiled_file)
			ram_file = NULL;
	} else {
		jsc_write_file(outfile, compiled_file);
	}

	return 0;
}

/**
 * create_ram_string() - Pad the bytecode with all zero cstrings
 *
 * @data: the bytecode
 *
 * We create however many lines of all zeros we need, using the global variable
 * %TOKEN_SIZE, which is set during compile_source().
 *
 * We then concatenate the bytecode with the created cstring.
 *
 * Return:
 * * a new, padded bytecode string
 * * the 'old', unpadded bytecode string, on failure
 */
char *create_ram_string(char *data)
{
	int len = divide(TOKEN_SIZE, 2);

	char *padding = NULL;
	if (len <= 999)
		padding = create_padding(1000 - len);

	if (padding) {
		char *buf = j_alloc(4096 * 5 + 1, sizeof(char));
		if (!buf) {
			return data;
		}

		strcpy(buf, data);
		strcat(buf, padding);
		buf[4096] = '\0';

		return buf;
	}

	return data;
}

/**
 * compile_source() - Turn a source file into a bytecode string
 *
 * @infile: the sourcefile
 * @just_check: Should we exit after error checking?
 * @dont_check: Should we check for errors?
 * @ignore: Shoule we ignore errors?
 *
 * We read the contents of @infile, create tokens from it, save the number of
 * tokens we have created to %TOKEN_SIZE and then create instructions from those
 * tokens, that are saved in an ast structure.
 *
 * We then use ast_compile() to get the bytecode string we wanted all along.
 *
 * Return:
 * * the bytecode string
 * * NULL on failure
 */
char *compile_source(char *infile, int just_check, int dont_check, int ignore)
{
	char *compiled_ast = NULL;
	char *input = jsc_read_file(infile);

	if (!input)
		goto invalid_infile;

	char **tokens = jsc_tokenize_input(input);

	if (!tokens)
		goto tokenize_failed;

	size_t tokenc = jsc_tokens_size(tokens);
	TOKEN_SIZE = tokenc;

	struct ast *a = ast_create(tokenc);

	if (!a)
		goto ast_failure;

	struct instruction *i;
	for (int j = 0; tokens[j] != NULL; j += 2) {
		if (tokens[j + 1] == NULL) {
			i = instruction_create(tokens[j], NULL);
		} else if (is_digit(tokens[j + 1])) {
			i = instruction_create(tokens[j], tokens[j + 1]);
		} else {
			i = instruction_create(tokens[j], NULL);
			--j;
		}

		if (!i)
			goto ast_failure;

		ast_add_instruction(a, i);
	}

	if (dont_check)
		goto skip_check;

	struct error_list *e = error_list_create();

	ast_check(a, e);

	if (e->len || just_check) {
		error_list_print(e);
		if (!ignore)
			goto found_errors;
	}

skip_check:
	compiled_ast = ast_compile(a);

	return compiled_ast;

found_errors:
ast_failure:
tokenize_failed:
invalid_infile:
	return NULL;
}

/**
 * parse() - Parse argv for any arguments
 *
 * @argc: Number of arguments
 * @argv: The Arguments
 *
 * We iterate over all arguments, look for those we recongize and set the properties
 * of the allocated options struct accordingly. If no outfile was set, use the
 * default of /dev/stdout and if pad was not given, keep it at 0. If no infile
 * was given use last_standalone() to find it as a positional argument instead.
 *
 * Return:
 * * the parsed options
 * * NULL on any error
 */
struct options *parse(int argc, char **argv)
{
	struct options *opts = j_alloc(1, sizeof(struct options));

	if (!opts) {
		perror("jsc - parse");

		return NULL;
	}

	int out_flag = 0;
	int in_flag = 0;

	char *err = NULL;

	for (int i = 0; i < argc; ++i) {
		if (CMP(argv[i], "-o", "--outfile")) {
			if (argc > (i + 1)) {
				opts->out = argv[++i];
				out_flag = 1;
			} else {
				err = "-o requires an argument";
				goto abort;
			}
		} else if (CMP(argv[i], "-i", "--infile")) {
			if (argc > (i + 1)) {
				opts->in = argv[++i];
				in_flag = 1;
			} else {
				err = "-i requires an argument";
				goto abort;
			}
		} else if (CMP(argv[i], "-p", "--pad")) {
			opts->pad = 1;
		} else if (CMP(argv[i], "-c", "--just-check")) {
			opts->just_check = 1;
			opts->dont_check = 0;
			opts->ignore = 0;
		} else if (CMP(argv[i], "-C", "--dont-check")) {
			opts->dont_check = 1;
			opts->ignore = 0;
			opts->just_check = 0;
		} else if (CMP(argv[i], "-I", "--ignore-errors")) {
			opts->ignore = 1;
			opts->just_check = 0;
			opts->dont_check = 0;
		} else if (CMP(argv[i], "-h", "--help")) {
			usage();
			goto abort;
		}
	}

	if (!out_flag)
		opts->out = "/dev/stdout";

	if (!in_flag) {
		opts->in = last_standalone(argc, argv);

		if (!opts->in) {
			usage();

			goto abort;
		}
	}

	return opts;

abort:
	if (err)
		fprintf(stderr, "%s\n", err);

	return NULL;
}

/**
 * last_standalone() - Return the last non-option argument
 *
 * @argc: Number of arguments
 * @argv: The arguments
 *
 * To find the last non-option argument, that is the last argument given to the
 * program that is not an option we parse for in parse(), we set a variable to
 * argv at an index, if they are not any option. This is horribly ineffective,
 * but works.
 *
 * Return:
 * * The last free standing argument
 * * NULL, if there isn't any
 */
char *last_standalone(int argc, char **argv)
{
	char *s = NULL;

	for (int i = 1; i < argc; ++i) {
		if (CMP(argv[i], "-o", "--outfile") ||
		    CMP(argv[i], "-i", "--infile")) {
			goto skip;
		} else if (CMP(argv[i], "-p", "--pad") ||
			   CMP(argv[i], "-I", "--ignore-errors") || 
			   CMP(argv[i], "-c", "--just-check") ||
			   CMP(argv[i], "-C", "--dont-check") ||
			   CMP(argv[i], "-h", "--help")) {
			continue;
		}

		s = argv[i];
		continue;
skip:
		++i;
	}

	return s;
}

void usage(void)
{
	fprintf(stderr, "jsc [OPTIONS] INFILE\n\nOptions:\n");
	fprintf(stderr, "-o OUTFULE, --outfile OUTFILE\tRedirect stdout to OUTFILE\n");
	fprintf(stderr, "-i INFILE, --infile INFILE\tRead instructions from INFILE\n");
	fprintf(stderr, "-c, --just-check\t\tDon't compile, only check for correctness\n");
	fprintf(stderr, "-C, --dont-check\t\tDon't check, only compile\n");
	fprintf(stderr, "-I, --ignore-errors\t\tCheck for correctness and compile, even if errors are found\n");
}

/**
 * main() - Entrypoint
 *
 * @argc: Number of arguments
 * @argv: The arguments
 *
 * Parse the options, call compile() with them and then return.
 *
 * Also call j_init() and j_free(), to allow for easy alloc'/free'ing
 * with our jalloc wrapper :)
 *
 * Return:
 * * 0 - Success
 * * 1 - Failure
 */
int main(int argc, char **argv)
{
	j_init();
	struct options *opts = parse(argc, argv);

	if (!opts)
		goto early_exit;

	compile(opts);

early_exit:
	j_free();
	return 0;
}
