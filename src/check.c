// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later
#include "check.h"
#include <stdlib.h>

/** check_address() - Check if an address is valid
 *
 * @l: Error list to save any eventual error
 * @line: Which line are we checking?
 * @ins: Which instruction are we checking
 * @addr: Which address are we checking
 *
 * If addr is -1 or greater than 999, we have an invalid address
 * and add an error to the list. The upper limit is a constraint
 * by the Johnny Simulator, since it hardcodes the upper limit
 * of lines that it will handle to i < 1000. Theoretically it
 * is also limited to base-10 numbers only, but since we use
 * is_digit() this is a non-issue.
 *
 * See also: is_digit()
 */
void check_address(struct error_list *l, ull line, int ins, int addr)
{
	struct error *e = NULL;
	if (ins == INS_HLT)
		return;

	if (addr == INVALID_TARGET || addr > 999)
		e = error_create(ERR_INVALID_ADDRESS, line, ins, addr);

	if (e)
		error_list_insert(l, e);
}

/**
 * check_opcode() - Check if an opcode is valid
 *
 * @l: Error list to save any eventual error
 * @line: Which line are we checking
 * @ins: Which instruction are we checking
 * @addr: Which address are we checking
 *
 * Only allow defined opocdes. We enforce that via checking if
 * the instruction is between INS_DATA and INS_SIZE (non-inclusive).
 */
void check_opcode(struct error_list *l, ull line, int ins, int addr)
{
	struct error *e = NULL;
	if (ins <= INS_DATA || ins >= INS_SIZE)
		e = error_create(ERR_INVALID_OPCODE, line, ins, addr);

	if (e)
		error_list_insert(l, e);
}


/**
 * check_target() - Check if we have a target
 *
 * @l: Error list to save any eventual error
 * @line: Which line are we checking
 * @ins: Which instruction are we checking
 * @addr: Which address are we checking
 *
 * Check if we have a target or if the current instruction
 * is HLT.
 */
void check_target(struct error_list *l, ull line, int ins, int addr)
{
	struct error *e = NULL;
	if (addr == NO_TARGET && ins != INS_HLT)
		e = error_create(ERR_NO_TARGET, line, ins, addr);

	if (e)
		error_list_insert(l, e);
}
