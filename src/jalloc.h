// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef JALLOC_H
#define JALLOC_H

#include "helpers.h"

/**
 * struct list_head - The allocation list head
 *
 * @nxt: The first jalloc struct
 * @len: Internal length count
 *
 * To save on calloc() this struct was created. It
 * just serves as entry point to the jalloc list.
 * @len is currently unused.
 */
struct list_head {
	struct jalloc *nxt;
	ull len;
};

/**
 * struct jalloc - Janitorial Alloc Struct
 *
 * @data: Pointer to memory
 * @nxt: Pointer to the next jalloc struct
 *
 * Yeah, just that. Nothing more.
 */
struct jalloc {
	void *data;
	struct jalloc *nxt;
};

struct jalloc *jalloc_create(void);
void jalloc_delete(struct jalloc *);
void jalloc_insert(struct jalloc *);

void *j_alloc(ull, ull);
void *j_realloc(void *, ull, ull);
void j_init(void);
void j_free(void);

#endif
