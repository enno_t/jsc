// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later
#include "check.h"
#include "helpers.h"
#include "jalloc.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * error_list_create() - Create an error_list struct
 *
 * Allocate the error_list and return it.
 *
 * Return:
 * * an error_list struct
 * * NULL on any error
 */
struct error_list *error_list_create(void)
{
	struct error_list *l = j_alloc(1, sizeof(struct error_list));

	if (!l) {
		perror("jsc - error_list_create");

		return NULL;
	}

	l->head = NULL;
	l->tail = NULL;
	l->len = 0;

	return l;
}

/**
 * error_list_insert() - Insert an error into the error_list
 *
 * @l: The error list
 * @e: The error
 *
 * Insert the error at the end of the error list
 */
void error_list_insert(struct error_list *l, struct error *e)
{
	if (!l->head) {
		l->head = e;
		l->tail = e;
		l->len = 1;
	} else {
		error_set_nxt(l->tail, e);
		l->tail = e;
		l->len += 1;
	}
}

/**
 * error_list_length() - Count the length of an error_list
 *
 * @l: The error list
 *
 * Count all errors in the error list
 *
 * Return: LEN(error_list)
 */
ull error_list_length(struct error_list *l)
{
	ull len = 0;
	struct error *y;

	error_list_foreach(l, y) {
		++len;
		y = y->nxt;
	}

	return len;
}

/**
 * error_list_to_str() - Turn all errors into strings
 *
 * @l: The error list
 *
 * Turn all errors into strings and store them in an
 * array.
 *
 * Return:
 * * An array of strings
 * * NULL on any error
 */
char **error_list_to_str(struct error_list *l)
{
	ull len = error_list_length(l);

	char **strs = j_alloc(len + 1, sizeof(char *));

	if (!strs) {
		perror("jsc - error_list_to_str");

		return NULL;
	}

	struct error *y;
	ull i = 0;

	error_list_foreach(l, y) {
		strs[i] = error_to_str(y);
		if (!strs[i]) {
			strs[i + 1] = NULL;
			return strs;
		}

		++i;
		y = y->nxt;
	}

	strs[len] = NULL;

	return strs;
}

/**
 * error_list_print() - Print all errors in the list
 *
 * @l: The error list
 *
 * Turn all errors into strings and then print them to stderr.
 */
void error_list_print(struct error_list *l)
{
	char **strs = error_list_to_str(l);

	if (!strs)
		return;

	for (ull i = 0; strs[i]; ++i) {
		fprintf(stderr, "%s\n", strs[i]);
	}
}

/**
 * error_create() - Create an error
 *
 * @kind: The error kind
 * @line: The current line
 * @ins: The current instruction
 * @target: The current target
 *
 * Create the error with the given information.
 *
 * Return:
 * * An error struct
 * * NULL on any error
 */
struct error *error_create(enum error_kind kind, ull line, int ins, int target)
{
	struct error *e = j_alloc(1, sizeof(struct error));

	if (!e) {
		perror("jsc - error_create");
		return NULL;
	}

	e->kind = kind;
	e->line = line;
	e->ins = ins;
	e->target = target;
	e->nxt = NULL;

	return e;
}

/**
 * error_to_str() - Turn _one_ error into a string
 *
 * @e: The error
 *
 * Turn the error into a string with the given error strings
 * suing sprintf().
 *
 * Return:
 * * A cstring
 * * NULL on any error
 */
char *error_to_str(struct error *e)
{
	char *buf = j_alloc(2048, sizeof(char));

	if (!buf)
		return NULL;

	switch (e->kind) {
	case ERR_INVALID_OPCODE:
		sprintf(buf,
			"Invalid Opcode at line %lld (ins: %d, target: %d)",
			e->line, e->ins, e->target);
		break;
	case ERR_INVALID_ADDRESS:
		sprintf(buf,
			"Invalid Address at line %lld (ins: %d, target: %d)",
			e->line, e->ins, e->target);
		break;
	case ERR_NO_TARGET:
		sprintf(buf,
			"Missing required target at line %lld (ins: %d, target: %d)",
			e->line, e->ins, e->target);
		break;
	default:
		sprintf(buf, "Unkown error at line %lld (ins: %d, target: %d)",
			e->line, e->ins, e->target);
		break;
	}

	return buf;
}


/**
 * error_set_nxt() - Set the next error
 *
 * @e: The error
 * @nxt: The next error
 *
 * We set the next error of a existing error and insert
 * @nxt between @e and its old next error if that one
 * exists.
 */
void error_set_nxt(struct error *e, struct error *nxt)
{
	if (!e->nxt) {
		e->nxt = nxt;
	} else {
		struct error *tmp = e->nxt;
		e->nxt = nxt;
		error_set_nxt(e->nxt, tmp);
	}
}
