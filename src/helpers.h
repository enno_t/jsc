// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef HELPERS_H
#define HELPERS_H

typedef unsigned long long ull;

#define FNV1A_PRIME 0x100000001b3
#define FNV1A_OFFSET 0xcbf29ce484222325
#define LINE_SIZE 16

// '/' and ':' are before/after the ASCII
// number block :)
#define IS_NUMBER(x) x > '/' && x < ':'
#define CMP(x, y, z) (!strcmp(x, y) || !strcmp(x, z))

int is_digit(char *);
char *create_padding(int);
int divide(int, int);
long fnv1a_hash(const char *);

#endif
