// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "jalloc.h"

#include <stdlib.h>

struct list_head HEAD = { .nxt = NULL, .len = -1 };

/**
 * j_init() - Useless function
 *
 * This function does nothing. :)
 */
void j_init(void)
{
	HEAD.len = 0;
}

/**
 * j_alloc() - Allocate some memory
 * 
 * @nmeb: Number of members
 * @size: Size of each member
 *
 * This wraps a call to calloc() with the janitorial
 * stuffs.
 *
 * Return:
 * * Pointer to the allocated memory
 * * NULL on any error
 */
void *j_alloc(ull nmeb, ull size)
{
	struct jalloc *j = jalloc_create();

	if (!j)
		return NULL;

	jalloc_insert(j);

	j->data = calloc(nmeb, size);

	return j->data;
}

/**
 * jalloc_create() - Create a jalloc struct
 *
 * Create the struct, set everything to NULL and
 * then return it.
 *
 * Return:
 * * A jalloc struct
 * * NULL on any error
 */
struct jalloc *jalloc_create(void)
{
	struct jalloc *j = calloc(1, sizeof(struct jalloc));

	if (!j)
		return NULL;

	j->data = NULL;
	j->nxt = NULL;

	return j;
}

/**
 * jalloc_insert() - Insert a jalloc struct into the list
 *
 * @j: The jalloc struct
 *
 * Figure out where to insert @j and put it there. We are
 * looking for the end, but are not keeping track of it,
 * so we have to always search for it.
 */
void jalloc_insert(struct jalloc *j)
{
	if (!HEAD.nxt) {
		HEAD.nxt = j;
		HEAD.len++;
		return;
	}

	struct jalloc *tmp = HEAD.nxt;

	while (tmp->nxt != NULL) {
		tmp = tmp->nxt;
	}

	HEAD.len++;
	tmp->nxt = j;
}

/**
 * jalloc_delete() - Free a jalloc struct
 *
 * @j: The jalloc struct
 *
 * First, set HEAD.nxt to @j->nxt (we always operate
 * on HEAD.nxt, so we can do that), then free the
 * data @j keeps track of and then free @j.
 */
void jalloc_delete(struct jalloc *j)
{
	HEAD.nxt = j->nxt;
	if (j->data)
		free(j->data);
	free(j);
}


/**
 * j_realloc() - Wrap a realloc() call
 *
 * @buf: The pointer returned by a call to j_alloc()
 * @nmeb: New number of members
 * @size: New size of each member
 *
 * We look for the jalloc struct that keeps track of the memory
 * at @buf and then call realloc().
 *
 * Return:
 * * A pointer to the new memory
 * * NULL on any error
 */
void *j_realloc(void *buf, ull nmeb, ull size)
{
	struct jalloc *tmp = HEAD.nxt;

	while (tmp && tmp->data != buf) {
		tmp = tmp->nxt;
	}

	if (tmp)
		tmp->data = realloc(tmp->data, nmeb * size);
	else
		return NULL;

	return (tmp->data) ? tmp->data : NULL;
}

/**
 * j_free() - Free everything we allocated
 *
 * Call jalloc_delete() on HEAD.nxt until HEAD.nxt is NULL.
 */
void j_free(void)
{
	while (HEAD.nxt) {
		jalloc_delete(HEAD.nxt);
	}
}
