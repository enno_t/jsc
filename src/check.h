// SPDX-FileCopyrightText: 2023 Enno Tensing <tenno@suij.in>
//
// SPDX-License-Identifier: GPL-3.0-or-later
#ifndef CHECK_H
#define CHECK_H

#include "asm.h"
#include "helpers.h"

#define error_list_foreach(x, y) \
	y = x->head;             \
	while (y)

/**
 * enum error_kind - All possible kinds of errors
 *
 * @ERR_INVALID_OPCODE: Unkown opcode
 * @ERR_INVALID_ADDRESS: Usually an address > 999
 * @ERR_NO_TARGET: No target was supplied, but instruction is not HLT
 */
enum error_kind { ERR_INVALID_OPCODE, ERR_INVALID_ADDRESS, ERR_NO_TARGET };

/**
 * struct error - everything needed for an error
 *
 * @kind: What type of error is this?
 * @line: At which line did it occur?
 * @ins: What is the instruction there?
 * @target: What is the argument there?
 * @nxt: The next error
 *
 * For simplicities sake this is a linked list node.
 */
struct error {
	enum error_kind kind;
	ull line;
	int ins;
	int target;
	struct error *nxt;
};


/**
 * error_list - head for all errors
 *
 * @head: The start of the list
 * @tail: The end of the list
 * @len: How many errors are there currently?
 */
struct error_list {
	struct error *head;
	struct error *tail;
	ull len;
};

struct error_list *error_list_create(void);
void error_list_insert(struct error_list *, struct error *);
char **error_list_to_str(struct error_list *);
ull error_list_length(struct error_list *);
void error_list_print(struct error_list *);

struct error *error_create(enum error_kind, ull, int, int);
void error_set_nxt(struct error *, struct error *);
char *error_to_str(struct error *);

void check_address(struct error_list *, ull, int, int);
void check_opcode(struct error_list *, ull, int, int);
void check_target(struct error_list *, ull, int, int);

#endif
